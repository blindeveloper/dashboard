const API_ROOT = 'http://localhost:3001/api/'

export const API = {
  PRODUCTS: `${API_ROOT}products/`,
  CUSTOMERS: `${API_ROOT}customers/`,
  BEST_CUSTOMERS: `${API_ROOT}customers/revenues/`,
  INCOICES: `${API_ROOT}invoices/`,
  REVENEUS: `${API_ROOT}revenues/`,
  BEST_PRODUCT_CATEGORIES: `${API_ROOT}categories/revenues/`
}

export const VALUE_TYPE = {
  REVENUE: "revenue",
  MARGIN: "margin"
}

export const PERIOD_TYPE = {
  MONTHLY: "monthly",
  WEEKLY: "weekly"
}

export const isMonthlyPeriod = (period:string):boolean => period === PERIOD_TYPE.MONTHLY
export const isWeeklyPeriod = (period:string):boolean => period === PERIOD_TYPE.WEEKLY

export const isRevenueValueType = (type:string):boolean => type === VALUE_TYPE.REVENUE
export const isMarginValueType = (type:string):boolean => type === VALUE_TYPE.MARGIN