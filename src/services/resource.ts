import { API } from "./const";
import { isEmptyArray, getQueryStringMerge, groupBy } from "./tools"
import { IBestCustomer } from "./interfaces"

async function getDataByUrl(url:string) {
  try {
    const response = await fetch(url, {
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      referrerPolicy: 'no-referrer'
    });
    return response.json();
  } catch(err) {
    throw new Error(err)
  }
}

export const getLatestInvoices = (limit:number, cb:(val:any) => void) => {
  getDataByUrl(`${API.INCOICES}?_sort=date&_order=desc&_limit=${limit}`).then(res => cb(res))
}

export const getInvoicesByCustumerIdList = (query:string, cb:(val:any) => void) => {
  getDataByUrl(`${API.INCOICES}?${query}`).then(res => cb(res))
}

export const getProductsCategories = (cb:(val:any) => void) => {
  getDataByUrl(API.BEST_PRODUCT_CATEGORIES).then(res => cb(res))
}

export const getBestCustomers = (cb:(val:any) => void) => {
  getDataByUrl(API.BEST_CUSTOMERS).then(res => cb(res))
}

export const getRevenues = (timePeriod:string, cb:(val:any) => void) => {
  getDataByUrl(`${API.REVENEUS}${timePeriod}`).then(res => cb(res))
  //TODO investigate why sorting doesnt work here
  // getDataByUrl(`${API.REVENEUS}${timePeriod}?_sort=month&_order=desc`).then(res => cb(res))
}

export const getBestCustomersWirtInvoices = (cb:(val:any) => void) => {
  getBestCustomers((bestCustomers) => {
    if (!isEmptyArray(bestCustomers)) {
      const besCustomersIdList:number[] = bestCustomers.map((customer:IBestCustomer) => customer.customer_id)
      const queryString:string = getQueryStringMerge(besCustomersIdList)
      getInvoicesByCustumerIdList(queryString, (invoices) => {
        const groupedInvoices:Record<string, any> = groupBy(invoices, 'customer_id')
        const bestCustomersWithInvoices2:any[] = bestCustomers.map((bestCustomer:IBestCustomer) => {
          return {
            ...bestCustomer, 
            invoices: groupedInvoices[bestCustomer.customer_id]
          }
        })
        return cb(bestCustomersWithInvoices2)
      })
    }
  })
}