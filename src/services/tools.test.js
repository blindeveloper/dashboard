import { getCumulativeData, getTime } from './tools'

const mock = [
  { total_margin: 1, total_revenue: 2, end_date: '2020-02-25' },
  { total_margin: 2, total_revenue: 3, end_date: '2020-01-12' },
  { total_margin: 5, total_revenue: 13, end_date: '2020-03-2' }
]

const result = [
  {
    total_margin: 2,
    total_revenue: 3,
    end_date: getTime('2020-01-12'),
    cumulative_margin: 2,
    cumulative_revenue: 3
  },
  {
    total_margin: 1,
    total_revenue: 2,
    end_date: getTime('2020-02-25'),
    cumulative_margin: 3,
    cumulative_revenue: 5
  },
  {
    total_margin: 5,
    total_revenue: 13,
    end_date: getTime('2020-03-2'),
    cumulative_margin: 8,
    cumulative_revenue: 18
  }
]

describe('Testing of getCumulativeData function', () => {
  it('should check  values', () => {
    expect(getCumulativeData(mock)).toEqual(result)  
  })
})