import { VALUE_TYPE, PERIOD_TYPE } from '../services/const'
import {IInvoice, IBestCustomer} from '../services/interfaces'

export const isEmptyArray = (array:any[]):boolean => {
  return !Array.isArray(array) || !array.length ? true : false;
}

export const getQueryStringMerge = (list:number[]):string => {
  let string = ''
  list.forEach(el => string += `&customer_id=${el}`)
  return string
}

export const groupBy = (array:any[], key:string):object => {
  return array.reduce((result, currentValue) => {
    (result[currentValue[key]] = result[currentValue[key]] || []).push(
      currentValue
    );
    return result;
  }, {});
};

export const getUniqueArray = (list:any[]) => [...new Set(list)]

export const getTime = (time:string) => new Date(time).getTime() 

export const formatTime = (dateTime:string) => {
  var date = new Date(dateTime);
  
  return date.toLocaleDateString('en', {
    month: 'numeric', 
    year: 'numeric', 
  });
}

export const getCumulativeData = (revenues:any[]) => {
  const sortedRevenues = revenues
  .map(revenue => ({ ...revenue, end_date: getTime(revenue.end_date)}))
  .sort((a, b) => a.end_date - b.end_date)//TODO reduce amount of loops when query with sort will start working

  const cumulativeSum = ((margin_sum, revenue_sum) => (el:any) => {
    return {
      ...el,
      cumulative_margin: margin_sum += el.total_margin,
      cumulative_revenue: revenue_sum += el.total_revenue,
    }
  })(0, 0);

  return sortedRevenues.map(cumulativeSum)
}

export const getBestCustomersWithRegion = (list: any[]) => {
  let result: any[] = [];
  list.forEach(customer => {
    let regionList = customer.invoices.map((customer:IInvoice) => customer.region);

    let customerWithRegions = {
      ...customer,
      regionList: getUniqueArray(regionList),
    };
    result.push(customerWithRegions);
  });

  return result;
};


export const capitalize = (s:string) => s && s[0].toUpperCase() + s.slice(1)

export const isMonthlyPeriod = (period:string):boolean => period === PERIOD_TYPE.MONTHLY
export const isWeeklyPeriod = (period:string):boolean => period === PERIOD_TYPE.WEEKLY

export const isRevenueValueType = (type:string):boolean => type === VALUE_TYPE.REVENUE
export const isMarginValueType = (type:string):boolean => type === VALUE_TYPE.MARGIN

export const getTopProducts = (bestCustomers:IBestCustomer[]) => {
  if (!isEmptyArray) return []
  let invoiceLines:any[] = []
  bestCustomers.forEach(customer => {
    customer.invoices.forEach(customerIvoice => {
      invoiceLines.push(...customerIvoice.invoice_lines)
    })
  })

  let topProducts:any = [];
  invoiceLines.reduce((res, value) => {
    if (!res[value.product_id]) {
      res[value.product_id] = { product_id: value.product_id, quantity: 0, product_name: value.product_name };
      topProducts.push(res[value.product_id])
    }
    res[value.product_id].quantity += value.quantity;
    return res;
  }, {});

  return topProducts.sort((a:any, b:any) => b.quantity - a.quantity)
}