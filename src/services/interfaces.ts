export interface ITopProductsProps {
  topProducts: ITopProduct[]
}
export interface ITopProduct {
  product_id: number
  product_name: string
  quantity: number
}
export interface IFilterProps {
  valueType:string
  setValueType: (status: string) => void
  period:string
  setPeriod: (status: string) => void
}
export interface ILatestInvoicesTableViewProps {
  latestInvoices: Array<IInvoice>
  valueType: string
}
export interface IBestCustomerTableViewProps {
  bestCustomers: Array<IBestCustomer> 
  valueType: string
}
export interface ICumulativeInvoicesRevenuesProps {
  period: string
  revenues: Array<any>
  handleSetRevenues: () => void
  valueType: string
}
export interface IProductsCategoriesProps {
  productsCategories: Array<IProductCategory>
  handleSetProductsCategories: () => void
  valueType: string
}
export interface ILatestInvoicesProps {
  limit: number
  handleSetLatestInvoices: (limit:number) => void
  latestInvoices: Array<IInvoice>
  valueType: string
}
export interface IInvoiceProps {
  invoice: IInvoice
  key: string
  valueType: string
}
export interface IValueTypeSelectorProps {
  setValueType: (status: string) => void
  valueType: string
}
export interface IPeriodSelectorProps {
  period: string
  setPeriod: (status: string) => void
}
export interface IBestCustomersProps {
  bestCustomers: Array<IBestCustomer> 
  handleSetBestCustomers: () => void
  valueType:string
}

export interface IBestCustomerProps {
  bestCustomer: IBestCustomer 
  key: string
  valueType: string
}

export interface IFlagProps {
  countryName: string
}
export interface IProduct {
  id: number
  category: string
  name: string
  sale_price: number
  margin: number
}
export interface IInvoice {
  id: number
  customer_id: number
  customer_name: string
  date: string
  total_invoice: number
  total_margin: number
  region:  string
  invoice_lines: [
    {
      product_id: number
      product_name: string
      unit_price: number
      quantity: number
      total_line: number
      total_margin: number
    }
  ]
}
export interface IBestCustomer {
  customer_id: number
  customer_name: string
  invoices_count: number
  total_margin: number
  total_revenue: number
  regionList: string[]
  invoices: IInvoice[]
}
export interface IBestCustomerWithRegions {
  customer_id: number
  customer_name: string
  invoices: IInvoice[]
  invoices_count: number
  regionList: string[]
  total_margin: number
  total_revenue: number
}

export interface IProductCategory {
  category_name: string
  total_revenue: number
  total_margin: number
}

export interface IRevenue {
  end_date: string
  invoices_count: number
  month: string
  start_date: string
  total_margin: number
  total_revenue: number
}