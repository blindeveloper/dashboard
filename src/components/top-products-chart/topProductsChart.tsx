import React from "react";
import { Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Bar } from "react-chartjs-2";
import {
  ITopProduct,
  ITopProductsProps,
} from "../../services/interfaces";
import { isEmptyArray } from "../../services/tools";

const TopProductsChart: React.FC<ITopProductsProps> = (props) => {
  const { topProducts } = props;
  const classes = useStyles();

  const getBarData = () => {
    let barLabels: string[] = [];
    let quantityList: number[] = [];

    topProducts.forEach((el: ITopProduct) => {
      barLabels.push(el.product_name);
      quantityList.push(el.quantity);
    });

    return {
      labels: barLabels,
      datasets: [
        {
          label: "Product",
          backgroundColor: "#ffecdb",
          borderColor: "#fea041",
          borderWidth: 1,
          hoverBackgroundColor: "#f9b678b5",
          hoverBorderColor: "#ffc692",
          data: quantityList
        },
      ],
    };
  };

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <Typography
        variant="h5"
        component="h2"
        className={classes.left}
        color="textSecondary"
        gutterBottom
        data-testid="latest-invoices-header"
      >
        {!isEmptyArray(topProducts)
          ? `Most popular sold products.`
          : "No data for showing top products chart yet."}
      </Typography>
      {!isEmptyArray(topProducts) ? <Bar data={getBarData()} /> : null}
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
  left: {
    textAlign: "left",
  },
}));

export default TopProductsChart;
