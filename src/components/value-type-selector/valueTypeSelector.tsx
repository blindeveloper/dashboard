import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Paper, Checkbox } from "@material-ui/core";
import React from "react";
import { IValueTypeSelectorProps } from "../../services/interfaces";
import { makeStyles } from "@material-ui/core/styles";
import {
  isRevenueValueType,
  isMarginValueType,
  capitalize,
} from "../../services/tools";
import { VALUE_TYPE } from "../../services/const";

const ValueTypeSelector: React.FC<IValueTypeSelectorProps> = (props) => {
  const { valueType, setValueType } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <FormControlLabel
        control={
          <Checkbox
            checked={isRevenueValueType(valueType)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              e.target.checked && setValueType(VALUE_TYPE.REVENUE);
            }}
            name={VALUE_TYPE.REVENUE}
          />
        }
        label={capitalize(VALUE_TYPE.REVENUE)}
      />
      <FormControlLabel
        control={
          <Checkbox
            checked={isMarginValueType(valueType)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              e.target.checked && setValueType(VALUE_TYPE.MARGIN);
            }}
            name={VALUE_TYPE.MARGIN}
          />
        }
        label={capitalize(VALUE_TYPE.MARGIN)}
      />
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
}));

export default ValueTypeSelector;
