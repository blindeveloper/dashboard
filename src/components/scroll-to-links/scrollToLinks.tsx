import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/core/styles";

const SimpleMenu: React.FC = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const linkList = [
    {
      url: "top-products-chart",
      label: "Most popular products chart",
    },
    {
      url: "cumulative-invoices-revenues",
      label: "Cumulative invoices revenues",
    },
    {
      url: "products-categories-chart",
      label: "Products categories chart",
    },
    {
      url: "best-customers",
      label: "Best customers",
    },
    {
      url: "latest-invoices",
      label: "Latest invoices",
    },
  ];

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button
        variant="outlined"
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MenuIcon />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {linkList.map((link) => (
          <a className={classes.link} href={`#${link.url}`} key={link.url}>
            <MenuItem onClick={handleClose}>{link.label}</MenuItem>
          </a>
        ))}
      </Menu>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: "fixed",
    zIndex: 2,
    top: theme.spacing(11),
    right: theme.spacing(4),
  },
  link: {
    textDecoration: "none",
    color: "#4a4949",
  },
}));

export default SimpleMenu;
