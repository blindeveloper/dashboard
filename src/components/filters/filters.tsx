import React, { useState } from "react";
import { Grid, Button } from "@material-ui/core";
import ValueTypeSelector from "../value-type-selector/valueTypeSelector";
import PeriodSelector from "../period-selector/periodSelector";
import FilterListIcon from "@material-ui/icons/FilterList";
import { makeStyles } from "@material-ui/core/styles";
import { IFilterProps } from "../../services/interfaces";

const Filters: React.FC<IFilterProps> = (props) => {
  const { valueType, setValueType, period, setPeriod } = props;
  const [isShowFilters, setIsShowFilters] = useState(false);
  const classes = useStyles();

  return (
    <React.Fragment>
      {isShowFilters ? (
        <Grid container spacing={2} className={classes.filters}>
          <Grid item xs={4}>
            <ValueTypeSelector
              valueType={valueType}
              setValueType={setValueType}
            />
          </Grid>
          <Grid item xs={4}>
            <PeriodSelector period={period} setPeriod={setPeriod} />
          </Grid>
        </Grid>
      ) : null}

      <Button
        variant="outlined"
        className={classes.toggleFilters}
        type="button"
        onClick={() => setIsShowFilters(!isShowFilters)}
      >
        <FilterListIcon />
      </Button>
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme) => ({
  filters: {
    position: "fixed",
    top: theme.spacing(2),
    left: theme.spacing(2),
  },
  toggleFilters: {
    position: "fixed",
    top: theme.spacing(4),
    right: theme.spacing(4),
  },
}));

export default Filters;
