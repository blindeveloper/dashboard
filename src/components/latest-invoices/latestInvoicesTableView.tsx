import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@material-ui/core";
import {
  ILatestInvoicesTableViewProps,
  IInvoice,
} from "../../services/interfaces";
import Flag from "../flag/flag";
import { isRevenueValueType } from "../../services/tools";

const useStyles = makeStyles({
  root: {
    boxShadow: "none",
  },
});

const LatestInvoicesTableView: React.FC<ILatestInvoicesTableViewProps> = (
  props
) => {
  const { valueType, latestInvoices } = props;
  const classes = useStyles();

  return (
    <Grid item>
      <TableContainer component={Paper} className={classes.root}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Date</TableCell>
              <TableCell align="right">Name</TableCell>
              <TableCell align="right">Region</TableCell>
              <TableCell align="right">Total {valueType}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {latestInvoices.map((invoice: IInvoice) => (
              <TableRow key={invoice.id}>
                <TableCell>{invoice.customer_id}</TableCell>
                <TableCell component="th" scope="row">
                  {invoice.date}
                </TableCell>
                <TableCell align="right">{invoice.customer_name}</TableCell>
                <TableCell align="right">
                  <Flag key={invoice.region} countryName={invoice.region} />
                </TableCell>
                <TableCell align="right">
                  {isRevenueValueType(valueType)
                    ? invoice.total_invoice.toFixed(2)
                    : invoice.total_margin.toFixed(2)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Grid>
  );
};

export default LatestInvoicesTableView;
