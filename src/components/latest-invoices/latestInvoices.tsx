import React, { useEffect } from "react";
import { IInvoice, ILatestInvoicesProps } from "../../services/interfaces";
import { Typography, Grid, Paper, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Invoice from "./invoice";
import LatestInvoicesTableView from "./latestInvoicesTableView";
import { isEmptyArray } from "../../services/tools";

const LatestInvoices: React.FC<ILatestInvoicesProps> = (props) => {
  const { limit, handleSetLatestInvoices, latestInvoices, valueType } = props;

  useEffect(() => {
    handleSetLatestInvoices && handleSetLatestInvoices(limit);
  }, []);

  const classes = useStyles();

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <Typography
        variant="h5"
        component="h2"
        className={classes.left}
        color="textSecondary"
        gutterBottom
        data-testid="latest-invoices-header"
      >
        {`${
          !isEmptyArray(latestInvoices)
            ? `${limit} latest invoices`
            : "No invoices yet."
        }`}
      </Typography>

      <Box
        component={Grid}
        display={{
          xs: "none",
          sm: "none",
          md: "block",
          lg: "block",
          xl: "block",
        }}
      >
        <LatestInvoicesTableView
          latestInvoices={latestInvoices}
          valueType={valueType}
        />
      </Box>

      <Box
        component={Grid}
        display={{
          xs: "block",
          sm: "block",
          md: "none",
          lg: "none",
          xl: "none",
        }}
      >
        <Grid container spacing={2}>
          {!isEmptyArray(latestInvoices)
            ? latestInvoices.map((invoice: IInvoice) => (
                <Invoice
                  invoice={invoice}
                  key={`card_invoice_${invoice.id}`}
                  valueType={valueType}
                />
              ))
            : null}
        </Grid>
      </Box>
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  left: {
    textAlign: "left",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
}));

export default LatestInvoices;
