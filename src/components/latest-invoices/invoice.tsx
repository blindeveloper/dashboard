import React from "react";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import { IInvoiceProps } from "../../services/interfaces";
import { makeStyles } from "@material-ui/core/styles";
import Flag from "../flag/flag";
import { isRevenueValueType } from "../../services/tools";

const Invoice: React.FC<IInvoiceProps> = (props) => {
  const { invoice, valueType } = props;
  const classes = useStyles();

  return (
    <Grid item xs={12} sm={6} md={6} lg={4} xl={3} className="invoice">
      <Card variant="outlined" square>
        <CardContent>
          <Typography component="p" color="textSecondary">
            Customer ID:{" "}
            <span
              data-testid="invoice-customer-id"
              className={classes.highlighted}
            >
              {invoice.customer_id}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            Date:{" "}
            <span data-testid="invoice-date" className={classes.highlighted}>
              {invoice.date}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            Customer name:{" "}
            <span
              data-testid="invoice-customer-name"
              className={classes.highlighted}
            >
              {invoice.customer_name}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            Region:{" "}
            <span data-testid="invoice-region" className={classes.highlighted}>
              <Flag countryName={invoice.region} />
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            <span data-testid="invoice-total-type">
              {`Total ${valueType}:`}{" "}
            </span>
            <span data-testid="invoice-total" className={classes.highlighted}>
              {`${invoice[
                isRevenueValueType(valueType) ? "total_invoice" : "total_margin"
              ].toFixed(2)} EUR`}
            </span>
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

const useStyles = makeStyles({
  left: {
    textAlign: "left",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  flag: {
    height: 12,
    padding: "0 5px",
  },
  highlighted: {
    color: "#4a4949",
  },
});

export default Invoice;
