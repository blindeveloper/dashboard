import { render } from '@testing-library/react'
import Invoice from './invoice'
import { VALUE_TYPE } from '../../services/const'

const mockList = [
  {
    props: {
      invoice: {
        id: 1,
        customer_id: 1,
        customer_name: 'Misha',
        date: '2020 12 1',
        total_invoice: 1432,
        total_margin: 3432,
        region: 'Europe',
      },
      valueType: VALUE_TYPE.REVENUE
    },
    expecting: [
      { testId: "invoice-customer-id", value: "1" },
      { testId: "invoice-customer-name", value: "Misha" },
      { testId: "invoice-region", value: "EU" },
      { testId: "invoice-total-type", value: "Total revenue: " },
      { testId: "invoice-total", value: "1432.00 EUR" },
      { testId: "invoice-date", value: "2020 12 1" },
    ]
  },
  {
    props: {
      invoice: {
        id: 2,
        customer_id: 2,
        customer_name: 'Moro',
        date: '2020 12 4',
        total_invoice: 13432,
        total_margin: 34342,
        region:  'Ukraine',
      },
      valueType: VALUE_TYPE.MARGIN
    },
    expecting: [
      { testId: "invoice-customer-id", value: "2" },
      { testId: "invoice-customer-name", value: "Moro" },
      { testId: "invoice-total-type", value: "Total margin: " },
      { testId: "invoice-total", value: "34342.00 EUR" },
      { testId: "invoice-date", value: "2020 12 4" },
    ]
  }
]

describe('---Invoice Component test---', () => {
  mockList.forEach(mock => {
    it('should check rendered values', () => {
      const { getByTestId } = render(<Invoice {...mock.props} />)
      mock.expecting.forEach(el => {
        expect(getByTestId(el.testId).innerHTML).toEqual(el.value)
      })
    })
  });
  
})