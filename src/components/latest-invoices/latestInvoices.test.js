import { render } from '@testing-library/react'
import LatestInvoices from './latestInvoices'

const props = {
  handleSetLatestInvoices: () => {},
  limit: 15,
  latestInvoices: [
    {
      id: 1,
      customer_id: 1,
      customer_name: 'string',
      date: 'string',
      total_invoice: 1,
      total_margin: 1,
      region:  'string',
    },
    {
      id: 2,
      customer_id: 2,
      customer_name: 'string',
      date: 'string',
      total_invoice: 1,
      total_margin: 1,
      region:  'string',
    },
  ]
}

const noInvoicesProps = {
  handleSetLatestInvoices: () => {},
  limit: 15,
  latestInvoices: []
}

describe('---Latest Invoices Component test---', () => {
  it('should render 2 invoice components', () => {
    const renderedComponent = render(<LatestInvoices {...props} />)
    const { container } = renderedComponent
    expect(container.querySelectorAll('.invoice').length).toEqual(2)
  })
  
  it('should render "No invoices yet." message when no empty invoices list', () => {
    const { getByTestId } = render(<LatestInvoices {...noInvoicesProps} />)
    expect(getByTestId("latest-invoices-header").innerHTML).toEqual("No invoices yet.")
  })
})