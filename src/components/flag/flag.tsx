import React from "react";
import { findFlagUrlByCountryName } from "country-flags-svg";
import { IFlagProps } from "../../services/interfaces";
import { makeStyles } from "@material-ui/core/styles";

const Flag: React.FC<IFlagProps> = (props) => {
  const { countryName } = props;
  const classes = useStyles();
  const flagUrl = findFlagUrlByCountryName(countryName);

  return (
    <React.Fragment>
      {flagUrl ? (
        <img src={flagUrl} alt={countryName} className={classes.flag} />
      ) : countryName === "Europe" ? (
        "EU"
      ) : (
        countryName
      )}
    </React.Fragment>
  );
};

const useStyles = makeStyles({
  flag: {
    height: 12,
    padding: "0 5px",
  },
});

export default Flag;
