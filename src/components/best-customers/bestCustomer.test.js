import { render } from '@testing-library/react'
import BestCustomer from './bestCustomer'
import { VALUE_TYPE } from '../../services/const'

const mockList = [
  {
    props: {
      bestCustomer: {
        customer_id: 2,
        customer_name: "Mike",
        invoices_count: 8,
        total_margin: 3334.8789,
        total_revenue: 4423.56757,
        regionList:['Europe'],
        invoices: [{}, {}]
      },
      valueType: VALUE_TYPE.REVENUE
    },
    expecting: [
      { testId: "best-customer-name", value: "Mike" },
      { testId: "best-customer-regions", value: "EU" },
      { testId: "best-customer-invoices-number", value: "2" },
      { testId: "best-customer-total-type", value: "Total revenue: " },
      { testId: "best-customer-total", value: "4423.57 EUR" },
    ]
  },
  {
    props: {
      bestCustomer: {
        customer_id: 4,
        customer_name: "Black",
        invoices_count: 82,
        total_margin: 33334.67878,
        total_revenue: 432423.678678,
        regionList:['Europe'],
        invoices: [{}, {}, {}]
      },
      valueType: VALUE_TYPE.MARGIN
    },
    expecting: [
      { testId: "best-customer-name", value: "Black" },
      { testId: "best-customer-regions", value: "EU" },
      { testId: "best-customer-invoices-number", value: "3" },
      { testId: "best-customer-total-type", value: "Total margin: " },
      { testId: "best-customer-total", value: "33334.68 EUR" },
    ]
  }
]

describe('---BestCustomer Component test---', () => {
  mockList.forEach(mock => {
    it('should check rendered values', () => {
      const { getByTestId } = render(<BestCustomer {...mock.props} />)
      mock.expecting.forEach(el => {
        expect(getByTestId(el.testId).innerHTML).toEqual(el.value)
      })
    })
  });
  
})