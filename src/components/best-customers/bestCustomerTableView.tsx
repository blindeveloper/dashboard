import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@material-ui/core";
import {
  IBestCustomerTableViewProps,
  IBestCustomer,
} from "../../services/interfaces";
import Flag from "../flag/flag";
import { isRevenueValueType } from "../../services/tools";

const useStyles = makeStyles({
  root: {
    boxShadow: "none",
  },
});

const BestCustomerTableView: React.FC<IBestCustomerTableViewProps> = (
  props
) => {
  const { bestCustomers, valueType } = props;
  const classes = useStyles();

  return (
    <Grid item>
      <TableContainer component={Paper} className={classes.root}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell align="right">Regions</TableCell>
              <TableCell align="right">Invoices</TableCell>
              <TableCell align="right">Total {valueType}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {bestCustomers.map((customer: IBestCustomer) => (
              <TableRow key={customer.customer_id}>
                <TableCell>{customer.customer_id}</TableCell>
                <TableCell component="th" scope="row">
                  {customer.customer_name}
                </TableCell>
                <TableCell align="right">
                  {customer.regionList.map((region) => (
                    <Flag key={region} countryName={region} />
                  ))}
                </TableCell>
                <TableCell align="right">{customer.invoices.length}</TableCell>
                <TableCell align="right">
                  {isRevenueValueType(valueType)
                    ? customer.total_revenue.toFixed(2)
                    : customer.total_margin.toFixed(2)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Grid>
  );
};

export default BestCustomerTableView;
