import React from "react";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import { IBestCustomerProps } from "../../services/interfaces";
import { makeStyles } from "@material-ui/core/styles";
import { isRevenueValueType } from "../../services/tools";
import Flag from "../flag/flag";

const BestCustomer: React.FC<IBestCustomerProps> = (props) => {
  const { bestCustomer, valueType } = props;
  const classes = useStyles();

  return (
    <Grid item xs={12} sm={6} md={4} lg={4} xl={3} className="best-customer">
      <Card className={classes.root} variant="outlined" square>
        <CardContent>
          <Typography component="p" color="textSecondary">
            Customer name:{" "}
            <span
              className={classes.highlighted}
              data-testid="best-customer-name"
            >
              {bestCustomer.customer_name}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            Regions:{" "}
            <span
              className={classes.highlighted}
              data-testid="best-customer-regions"
            >
              {bestCustomer.regionList.map((region) => (
                <Flag key={region} countryName={region} />
              ))}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            Number of invoices:{" "}
            <span
              className={classes.highlighted}
              data-testid="best-customer-invoices-number"
            >
              {bestCustomer.invoices.length}
            </span>
          </Typography>
          <Typography component="p" color="textSecondary">
            <span data-testid="best-customer-total-type">
              {`Total ${valueType}:`}{" "}
            </span>
            <span
              className={classes.highlighted}
              data-testid="best-customer-total"
            >
              {`${bestCustomer[
                isRevenueValueType(valueType) ? "total_revenue" : "total_margin"
              ].toFixed(2)} EUR`}
            </span>
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    marginBottom: theme.spacing(2),
  },
  left: {
    textAlign: "left",
  },
  pos: {
    marginBottom: 12,
  },
  flag: {
    height: 12,
    padding: "0 5px",
  },
  highlighted: {
    color: "#4a4949",
  },
}));

export default BestCustomer;
