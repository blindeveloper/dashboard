import React, { useEffect } from "react";
import { Paper, Typography, Grid, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { IBestCustomersProps, IBestCustomer } from "../../services/interfaces";
import { isEmptyArray } from "../../services/tools";
import BestCustomer from "./bestCustomer";
import BestCustomerTableView from "./bestCustomerTableView";

const BestCustomers: React.FC<IBestCustomersProps> = (props) => {
  const { bestCustomers, handleSetBestCustomers, valueType } = props;
  const classes = useStyles();

  useEffect(() => {
    handleSetBestCustomers();
  }, []);

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <Typography
        variant="h5"
        component="h2"
        className={classes.left}
        color="textSecondary"
        gutterBottom
        data-testid="best-customers-header"
      >
        {`${
          !isEmptyArray(bestCustomers)
            ? "Best customers"
            : "No best customers yet."
        }`}
      </Typography>

      <Box
        component={Grid}
        display={{
          xs: "none",
          sm: "none",
          md: "block",
          lg: "block",
          xl: "block",
        }}
      >
        <BestCustomerTableView
          bestCustomers={bestCustomers}
          valueType={valueType}
        />
      </Box>

      <Box
        component={Grid}
        display={{
          xs: "block",
          sm: "block",
          md: "none",
          lg: "none",
          xl: "none",
        }}
      >
        <Grid container spacing={2}>
          {!isEmptyArray(bestCustomers)
            ? bestCustomers.map((customer: IBestCustomer) => (
                <BestCustomer
                  bestCustomer={customer}
                  valueType={valueType}
                  key={`card_customer_${customer.customer_id}`}
                />
              ))
            : null}
        </Grid>
      </Box>
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
  left: {
    textAlign: "left",
  },
}));

export default BestCustomers;
