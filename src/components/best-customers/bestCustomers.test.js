import { render } from '@testing-library/react'
import BestCustomers from './bestCustomers'
import { VALUE_TYPE } from '../../services/const'

const props = {
  bestCustomers: [
    {
      customer_id: 2,
      customer_name: "Mike",
      invoices_count: 8,
      total_margin: 3334,
      total_revenue: 4423,
      regionList:['Europe'],
      invoices: []
    }
  ],
  handleSetBestCustomers: () => {},
  valueType: VALUE_TYPE.REVENUE
}

const noBestCustomersProps = {
  bestCustomers: [],
  handleSetBestCustomers: () => {},
  valueType: VALUE_TYPE.MARGIN
}

describe('---Best Customers Component test---', () => {
  it('should render 2 custumer components', () => {
    const renderedComponent = render(<BestCustomers {...props} />)
    const { container } = renderedComponent
    expect(container.querySelectorAll('.best-customer').length).toEqual(1)
  })
  
  it('should render "No best customers yet." message when no best customers', () => {
    const { getByTestId } = render(<BestCustomers {...noBestCustomersProps} />)
    expect(getByTestId("best-customers-header").innerHTML).toEqual("No best customers yet.")
  })
})