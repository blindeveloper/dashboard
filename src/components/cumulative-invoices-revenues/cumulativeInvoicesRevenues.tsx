import React, { useEffect } from "react";
import { Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { ICumulativeInvoicesRevenuesProps } from "../../services/interfaces";
import {
  isEmptyArray,
  getCumulativeData,
  capitalize,
  isMonthlyPeriod,
  isRevenueValueType,
} from "../../services/tools";
import { Line } from "react-chartjs-2";

const CumulativeInvoicesRevenues: React.FC<ICumulativeInvoicesRevenuesProps> = (
  props
) => {
  const { period, revenues, handleSetRevenues, valueType } = props;
  const classes = useStyles();

  const getLineData = () => {
    const cumulativeRevenues = getCumulativeData(revenues);
    let lineLabels: string[] = [];
    let marginData: number[] = [];
    let reveneuData: number[] = [];

    cumulativeRevenues.forEach((el) => {
      lineLabels.push(isMonthlyPeriod(period) ? el.month : el.week);
      marginData.push(el.cumulative_margin);
      reveneuData.push(el.cumulative_revenue);
    });

    return {
      labels: lineLabels,
      datasets: [
        {
          label: `Cumulative ${valueType}`,
          data: isRevenueValueType(valueType) ? reveneuData : marginData,
          fill: true,
          borderWidth: 1,
          backgroundColor: "rgba(75,192,192,0.2)",
          borderColor: "rgba(75,192,192,1)",
        },
      ],
    };
  };

  useEffect(() => handleSetRevenues(), []);

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <Typography
        variant="h5"
        component="h2"
        className={classes.left}
        color="textSecondary"
        gutterBottom
        data-testid="latest-invoices-header"
      >
        {!isEmptyArray(revenues)
          ? `${capitalize(period)} cumulative invoices ${valueType}`
          : "No data for showing cumulative invoices revenues chart yet."}
      </Typography>
      <Line data={getLineData()} />
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
  left: {
    textAlign: "left",
  },
}));

export default CumulativeInvoicesRevenues;
