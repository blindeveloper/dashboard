import React, { useEffect } from "react";
import { Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Bar } from "react-chartjs-2";
import {
  IProductCategory,
  IProductsCategoriesProps,
} from "../../services/interfaces";
import { isEmptyArray, isMarginValueType } from "../../services/tools";

const ProductsCategoriesChart: React.FC<IProductsCategoriesProps> = (props) => {
  const { productsCategories, handleSetProductsCategories, valueType } = props;
  const classes = useStyles();

  useEffect(() => {
    handleSetProductsCategories();
  }, []);

  const getBarData = () => {
    let barLabels: string[] = [];
    let marginData: number[] = [];
    let reveneuData: number[] = [];

    productsCategories.forEach((el: IProductCategory) => {
      barLabels.push(el.category_name);
      marginData.push(el.total_margin);
      reveneuData.push(el.total_revenue);
    });
    return {
      labels: barLabels,
      datasets: [
        {
          label: "Products categories",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,0.4)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: isMarginValueType(valueType) ? marginData : reveneuData,
        },
      ],
    };
  };

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <Typography
        variant="h5"
        component="h2"
        className={classes.left}
        color="textSecondary"
        gutterBottom
        data-testid="latest-invoices-header"
      >
        {!isEmptyArray(productsCategories)
          ? `Total ${valueType} per products categories`
          : "No data for showing products categories chart yet."}
      </Typography>
      {!isEmptyArray(productsCategories) ? <Bar data={getBarData()} /> : null}
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
  left: {
    textAlign: "left",
  },
}));

export default ProductsCategoriesChart;
