import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Paper, Checkbox } from "@material-ui/core";
import React from "react";
import { IPeriodSelectorProps } from "../../services/interfaces";
import { makeStyles } from "@material-ui/core/styles";
import { isMonthlyPeriod, isWeeklyPeriod } from "../../services/tools";

const PeriodSelector: React.FC<IPeriodSelectorProps> = (props) => {
  const { period, setPeriod } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.paper} variant="outlined" square>
      <FormControlLabel
        control={
          <Checkbox
            name="monthly"
            checked={isMonthlyPeriod(period)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              e.target.checked && setPeriod("monthly");
            }}
          />
        }
        label="Monthly"
      />
      <FormControlLabel
        control={
          <Checkbox
            name="weekly"
            checked={isWeeklyPeriod(period)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              e.target.checked && setPeriod("weekly");
            }}
          />
        }
        label="Weekly"
      />
    </Paper>
  );
};

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(2),
  },
}));

export default PeriodSelector;
