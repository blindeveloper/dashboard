import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Box } from "@material-ui/core";
import LatestInvoices from "./components/latest-invoices/latestInvoices";
import {
  getLatestInvoices,
  getBestCustomersWirtInvoices,
  getProductsCategories,
  getRevenues,
} from "./services/resource";
import BestCustomers from "./components/best-customers/bestCustomers";
import ProductsCategoriesChart from "./components/products-categories-chart/productsCategoriesChart";
import ScrollToLinks from "./components/scroll-to-links/scrollToLinks";
import CumulativeInvoicesRevenues from "./components/cumulative-invoices-revenues/cumulativeInvoicesRevenues";
import { isEmptyArray, getBestCustomersWithRegion, getTopProducts } from "./services/tools";
import { VALUE_TYPE, PERIOD_TYPE } from "./services/const";
import Filters from "./components/filters/filters";
import TopProductsChart from './components/top-products-chart/topProductsChart'
import { IInvoice, IBestCustomer,IProductCategory, IRevenue, ITopProduct } from './services/interfaces'

const App: React.FC = () => {
  const classes = useStyles();
  const [latestInvoices, setLatestInvoices] = useState<IInvoice[]>([]);
  const [bestCustomers, setBestCustomers] = useState<IBestCustomer[]>([]);
  const [productsCategories, setProductsCategories] = useState<IProductCategory[]>([]);
  const [revenues, setRevenues] = useState<IRevenue[]>([]);
  const [period, setPeriod] = useState<string>(PERIOD_TYPE.MONTHLY);
  const [valueType, setValueType] = useState<string>(VALUE_TYPE.REVENUE);
  const [topProducts, setTopProducts] = useState<ITopProduct[]>([]);

  useEffect(() => {
    const products = getTopProducts(bestCustomers)
    if(!isEmptyArray(products)) setTopProducts(products)
    
  }, [bestCustomers])

  const handleSetLatestInvoices = (limit: number) => {
    getLatestInvoices(
      limit,
      (res) => !isEmptyArray(res) && setLatestInvoices(res)
    );
  };

  const handleSetRevenues = () => {
    getRevenues(period, (res) => !isEmptyArray(res) && setRevenues(res));
  };

  const handleSetBestCustomers = () => {
    getBestCustomersWirtInvoices((res) => {
      const bestCustomersWithRegion: any = getBestCustomersWithRegion(res);
      !isEmptyArray(bestCustomersWithRegion) &&
        setBestCustomers(bestCustomersWithRegion);
    });
  };

  const handleSetProductsCategories = () => {
    getProductsCategories((res) => {
      !isEmptyArray(res) && setProductsCategories(res);
    });
  };

  useEffect(() => handleSetRevenues(), [period]);

  return (
    <div className={classes.root}>
      <Box
        component={Grid}
        display={{
          xs: "block",
          sm: "block",
          md: "none",
          lg: "none",
          xl: "none",
        }}
      >
        <ScrollToLinks />
      </Box>

      <Filters
        valueType={valueType}
        setValueType={setValueType}
        period={period}
        setPeriod={setPeriod}
      />

      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div id="top-products-chart"></div>
          <TopProductsChart
            topProducts={topProducts}
          />
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={12} md={6} xl={3}>
          <div id="cumulative-invoices-revenues"></div>
          <CumulativeInvoicesRevenues
            period={period}
            revenues={revenues}
            handleSetRevenues={handleSetRevenues}
            valueType={valueType}
          />
        </Grid>
        <Grid item xs={12} md={6} xl={3}>
          <div id="products-categories-chart"></div>
          <ProductsCategoriesChart
            productsCategories={productsCategories}
            handleSetProductsCategories={handleSetProductsCategories}
            valueType={valueType}
          />
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <div id="best-customers"></div>
        <Grid item md={12} lg={6}>
          <BestCustomers
            bestCustomers={bestCustomers}
            handleSetBestCustomers={handleSetBestCustomers}
            valueType={valueType}
          />
        </Grid>
        <div id="latest-invoices"></div>
        <Grid item md={12} lg={6}>
          <LatestInvoices
            limit={15}
            handleSetLatestInvoices={handleSetLatestInvoices}
            latestInvoices={latestInvoices}
            valueType={valueType}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default App;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  filters: {
    position: "fixed",
    top: theme.spacing(2),
    left: 0,
  },
  toggleFilters: {
    position: "fixed",
    top: theme.spacing(4),
    right: theme.spacing(4),
  },
}));
